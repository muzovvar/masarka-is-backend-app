FROM openjdk:17-jdk-alpine

# set time zone to Europe/Prague
ENV TZ=Europe/Prague
RUN apk add tzdata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY ./REG_masarka-11a.pdf .
COPY ./ccll.ttf .

ADD /target/*.jar masarka-is-0.0.1.jar

ENTRYPOINT ["java","-jar","/masarka-is-0.0.1.jar"]