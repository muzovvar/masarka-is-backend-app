package cz.cvut.mk.is.service;

import cz.cvut.mk.is.models.Registration;
import cz.cvut.mk.is.repository.RegistrationRepository;
import cz.cvut.mk.is.repository.RoleRepository;
import cz.cvut.mk.is.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class RegistrationService {

    @Autowired
    RegistrationRepository registrationRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public List<Registration> findAll() {
        return this.registrationRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    public Registration create(Registration registration) {

        // check if user with given email already exist
        if (userRepository.existsByEmail(registration.getEmail())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email is already in use");
        }

        registration.setRegistrationDatetime(LocalDateTime.now());

        // save and return registration
        return registrationRepository.save(registration);


    }

    public Registration update(Registration oldRegistration, Registration newRegistration) {
        oldRegistration.setName(newRegistration.getName());
        oldRegistration.setSurname(newRegistration.getSurname());
        oldRegistration.setEmail(newRegistration.getEmail());
        oldRegistration.setDateOfBirth(newRegistration.getDateOfBirth());
        oldRegistration.setPlaceOfBirth(newRegistration.getPlaceOfBirth());
        oldRegistration.setPermanentAddress(newRegistration.getPermanentAddress());
        oldRegistration.setRegistrationDatetime(newRegistration.getRegistrationDatetime());
        oldRegistration.setRoomNumber(newRegistration.getRoomNumber());

        return this.registrationRepository.save(oldRegistration);
    }

}
