package cz.cvut.mk.is.service;

import cz.cvut.mk.is.exceptions.CreateNewDeviceException;
import cz.cvut.mk.is.exceptions.CreateNewReservationException;
import cz.cvut.mk.is.models.Device;
import cz.cvut.mk.is.models.Reservation;
import cz.cvut.mk.is.models.User;
import cz.cvut.mk.is.payload.request.account.UpdateProfileRequest;
import cz.cvut.mk.is.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountService {

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    ReservationService reservationService;

    @Autowired
    ReservationItemRepository reservationItemRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public User updateUser(User user, UpdateProfileRequest updateProfileRequest) {
        if (updateProfileRequest.getEmail() != null) {
            user.setEmail(updateProfileRequest.getEmail());
        }

        if (updateProfileRequest.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(updateProfileRequest.getPassword()));
        }

        if (updateProfileRequest.getRoomNumber() != null) {
            user.setRoomNumber(updateProfileRequest.getRoomNumber());
        }

        return this.userService.update(user);
    }

    public int getNumberOfReservationsInRange(List<Reservation> reservations, LocalDateTime start, LocalDateTime end) {
        // filter only items that intersect the given start/end range
        return (int) reservations.stream().filter(
                item -> start.isBefore(item.getEnd()) && end.isAfter(item.getStart())
        ).count();
    }

    public Reservation createReservation(User user, Reservation reservation)
            throws CreateNewReservationException, MessagingException {

        // if user is not active, throw exception
        if (!user.isActive()) {
            throw new CreateNewReservationException("Reservations can be created only by users with valid membership fee");
        }

        // if selected reservation item is not active, throw exception
        if (!reservation.getReservationItem().isActive()) {
            throw new CreateNewReservationException("Selected reservation item is not active");
        }

        if (reservation.getStart().isAfter(reservation.getEnd())) {
            throw new CreateNewReservationException("Incorrect time range selected");
        }

        // check if reservation is not in past
        if (ChronoUnit.HOURS.between(reservation.getStart(), LocalDateTime.now()) > 0) {
            throw new CreateNewReservationException("Can't make reservation in past");
        }

        // check if max duration is not exceeded
        if (Math.abs(ChronoUnit.HOURS.between(reservation.getStart(), reservation.getEnd())) >
                reservation.getReservationItem().getMaxDuration() ||
                Math.abs(ChronoUnit.HOURS.between(reservation.getStart(), reservation.getEnd())) < 1) {
            throw new CreateNewReservationException("Maximal duration exceeded");
        }

        // find all reservations for the given item
        List<Reservation> reservations = this.reservationRepository.findAllByReservationItem(
                reservation.getReservationItem());

        // filter only items that intersect the given start/end range
        reservations = reservations.stream().filter(
                item -> reservation.getStart().isBefore(item.getEnd()) &&
                        reservation.getEnd().isAfter(item.getStart())
        ).collect(Collectors.toList());

        // check if user already has reservation in selected time slot
        if (reservations.stream().filter(item -> user.equals(item.getUser())).count() > 0) {
            throw new CreateNewReservationException("You already have reservation in selected time slot");
        }

        // check for each hour if max concurrency is not exceeded
        for (LocalDateTime dateTime = reservation.getStart(); dateTime.isBefore(reservation.getEnd()); dateTime = dateTime.plusHours(1)) {
            if (getNumberOfReservationsInRange(reservations, dateTime, dateTime.plusHours(1)) >=
                    reservation.getReservationItem().getMaxConcurrency()) {
                throw new CreateNewReservationException("Selected time slot is not free");
            }
        }

        // set logged in user
        reservation.setUser(user);

        // save reservation to database
        return this.reservationService.create(reservation);
    }

    public Device createUserDevice(User user, Device device) throws CreateNewDeviceException {
        List<Device> devices = this.deviceRepository.findAllByUser(user);

        if (devices.size() >= 3) {
            throw new CreateNewDeviceException("User can add maximum three devices");
        } else {
            device.setUser(user);
            device.setActive(user.isActive());
            return this.deviceRepository.save(device);
        }
    }
}
