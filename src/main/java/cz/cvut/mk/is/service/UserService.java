package cz.cvut.mk.is.service;

import cz.cvut.mk.is.models.*;
import cz.cvut.mk.is.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static net.gcardone.junidecode.Junidecode.unidecode;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    TemplateEngine templateEngine;

    @Autowired
    JavaMailSender javaMailSender;

    @Cacheable(value = "users")
    public List<User> findAll() {
        System.out.println("### UserService.findAll()");
        return this.userRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    @CacheEvict(value = "users", allEntries = true)
    public User create(User user) throws Exception {

        if (user.getUsername() == null) {
            // generate new username based on name and surname
            user.setUsername(this.createUsername(user.getName(), user.getSurname()));
        }

        // check if username is already taken
        if (userRepository.existsByUsername(user.getUsername())) {
            throw new Exception("Username is already taken");
        }

        // check if email is already in use
        if (userRepository.existsByEmail(user.getEmail())) {
            throw new Exception("Email is already in use");
        }

        // get requested user roles
        Set<Role> requestRoles = user.getRoles();

        // create new empty roles set
        Set<Role> roles = new HashSet<>();

        if (requestRoles == null || requestRoles.isEmpty()) {
            // if no role is set, assign 'ROLE_USER' to user
            Role userRole = roleRepository.findByName("ROLE_USER")
                    .orElseThrow(() -> new Exception("Role 'ROLE_USER' is not found"));
            roles.add(userRole);

        } else {
            // iterate each requested role and check if it exists
            requestRoles.forEach(role -> {
                roles.add(roleRepository.findByName(role.getName())
                        .orElseThrow(() -> new ResponseStatusException(
                                HttpStatus.BAD_REQUEST, "Requested role is not found")));
            });
        }

        // set user roles before saving
        user.setRoles(roles);

        // set registration datetime
        user.setRegistrationDatetime(LocalDateTime.now());

        // make user active
        user.setActive(true);

        // save user to database
        user = this.userRepository.save(user);

        // set assigned user ID as an initial password
        user.setPassword(this.passwordEncoder.encode(user.getId().toString()));

        // save and return user
        user = userRepository.save(user);

        // send registration confirmation email
        this.sendRegistrationConfirmationEmail(user);

        return user;
    }

    @CacheEvict(value = "users", allEntries = true)
    public User update(User oldUser, User newUser) {
        oldUser.setActive(newUser.isActive());
        oldUser.setName(newUser.getName());
        oldUser.setSurname(newUser.getSurname());
        oldUser.setUsername(newUser.getUsername());
        oldUser.setEmail(newUser.getEmail());
        oldUser.setDateOfBirth(newUser.getDateOfBirth());
        oldUser.setPlaceOfBirth(newUser.getPlaceOfBirth());
        oldUser.setPermanentAddress(newUser.getPermanentAddress());
        oldUser.setRegistrationDatetime(newUser.getRegistrationDatetime());
        oldUser.setRoomNumber(newUser.getRoomNumber());
        oldUser.setRoles(newUser.getRoles());
        return this.userRepository.save(oldUser);
    }

    @CacheEvict(value = "users", allEntries = true)
    public User update(User user) {
        return this.userRepository.save(user);
    }

    @CacheEvict(value = "users", allEntries = true)
    public User resetPassword(User user) {
        // set assigned user ID as an initial password
        user.setPassword(this.passwordEncoder.encode(user.getId().toString()));
        return this.userRepository.save(user);
    }

    @CacheEvict(value = "users", allEntries = true)
    public void delete(User user) {
        List<Device> devices = this.deviceRepository.findAllByUser(user);
        List<Payment> payments = this.paymentRepository.findAllByUser(user);
        List<Reservation> reservations = this.reservationRepository.findAllByUser(user);

        for (Payment payment : payments) {
            payment.setUser(null);
        }

        this.paymentRepository.saveAll(payments);
        this.reservationRepository.deleteAll(reservations);
        this.deviceRepository.deleteAll(devices);
        this.userRepository.delete(user);
    }

    public String createUsername(String name, String surname) {
        String username = unidecode(name).replaceAll("[^A-Za-z]", "").toLowerCase() +
                '.' + unidecode(surname).replaceAll("[^A-Za-z]", "").toLowerCase();

        if (this.userRepository.existsByUsername(username)) {
            for (int i = 2; i < 100; i++) {
                if (!this.userRepository.existsByUsername(username + Integer.toString(i))) {
                    username = username + Integer.toString(i);
                    break;
                }
            }
        }

        return username;
    }

    public void sendRegistrationConfirmationEmail(User user) throws MessagingException {
        // create thymeleaf context object
        Context context = new Context();
        context.setVariable("user", user);

        // process email template
        String emailText = templateEngine.process("emails/registration-confirmation", context);

        // create new email message object
        javax.mail.internet.MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setSubject("Masarka Club Registration");
        helper.setText(emailText, true);
        helper.setTo(user.getEmail());

        // send email message
        javaMailSender.send(mimeMessage);
    }

    public void sendPasswordResetEmail(User user, String token) throws MessagingException {
        // create thymeleaf context object
        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("url", "https://is.mk.cvut.cz/reset_password?token=" + token);

        // process email template
        String emailText = templateEngine.process("emails/password-reset", context);

        // create new email message object
        javax.mail.internet.MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setSubject("[Masarka Club] Password reset");
        helper.setText(emailText, true);
        helper.setTo(user.getEmail());

        // send email message
        javaMailSender.send(mimeMessage);
    }

}
