package cz.cvut.mk.is.service;

import cz.cvut.mk.is.models.Ban;
import cz.cvut.mk.is.repository.BanRepository;
import org.springframework.stereotype.Service;

@Service
public class BanService {

    final
    BanRepository banRepository;

    public BanService(BanRepository banRepository) {
        this.banRepository = banRepository;
    }

    public Ban update(Ban oldBan, Ban newBan) {
        oldBan.setStart(newBan.getStart());
        oldBan.setEnd(newBan.getEnd());
        oldBan.setUser(newBan.getUser());
        oldBan.setNote(newBan.getNote());
        return this.banRepository.save(oldBan);
    }
}
