package cz.cvut.mk.is.service;

import cz.cvut.mk.is.models.Device;
import cz.cvut.mk.is.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DeviceService {

    DeviceRepository deviceRepository;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    public Device update(Device oldDevice, Device newDevice) {
        oldDevice.setName(newDevice.getName());
        oldDevice.setType(newDevice.getType());
        oldDevice.setActive(newDevice.isActive());
        oldDevice.setMacAddress(newDevice.getMacAddress());
        oldDevice.setUser(newDevice.getUser());
        return this.deviceRepository.save(oldDevice);
    }

    public Device update(Device device) {
        return this.deviceRepository.save(device);
    }

}