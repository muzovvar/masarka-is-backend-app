package cz.cvut.mk.is.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.mk.is.models.Payment;
import cz.cvut.mk.is.models.User;
import cz.cvut.mk.is.repository.PaymentRepository;
import cz.cvut.mk.is.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    UserRepository userRepository;

    @Value("${app.auth.bank-api-token}")
    private String BANK_API_TOKEN;

    public Payment update(Payment oldPayment, Payment newPayment) {
        oldPayment.setDate(newPayment.getDate());
        oldPayment.setAmount(newPayment.getAmount());
        oldPayment.setAccountNumber(newPayment.getAccountNumber());
        oldPayment.setBankCode(newPayment.getBankCode());
        oldPayment.setVariableSymbol(newPayment.getVariableSymbol());
        oldPayment.setConstantSymbol(newPayment.getConstantSymbol());
        oldPayment.setMessageForRecipient(newPayment.getMessageForRecipient());
        oldPayment.setNote(newPayment.getNote());
        oldPayment.setUser(newPayment.getUser());
        return this.paymentRepository.save(oldPayment);
    }

    public void fetchPaymentsFromBankAPI() throws JsonProcessingException {

        RestTemplate restTemplate = new RestTemplate();
        String url = "https://www.fio.cz/ib_api/rest/periods/" + BANK_API_TOKEN + "/2021-07-01/2121-01-01/transactions.json";
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());
        JsonNode transactions = root.path("accountStatement").path("transactionList").path("transaction");

        for (JsonNode transaction : transactions) {
            Payment payment = parseTransactionNode(transaction);

            if (!paymentRepository.existsByTransactionId(payment.getTransactionId())) {
                paymentRepository.save(payment);
            }
        }
    }

    public Payment parseTransactionNode(JsonNode transaction) {

        Payment payment = new Payment();

        for (JsonNode node : transaction) {
            if (!node.isEmpty()) {
                String name = node.get("name").textValue();

                if (name.equals("ID pohybu")) {
                    payment.setTransactionId(node.get("value").asLong());
                    continue;
                }

                if (name.equals("Datum")) {
                    payment.setDate(LocalDate.parse(node.get("value").asText().substring(0, 10)));
                    continue;
                }

                if (name.equals("Objem")) {
                    payment.setAmount(node.get("value").asDouble());
                    continue;
                }

                if (name.equals("Protiúčet")) {
                    payment.setAccountNumber(node.get("value").asText());
                    continue;
                }

                if (name.equals("Kód banky")) {
                    payment.setBankCode(node.get("value").asText());
                    continue;
                }

                if (name.equals("VS")) {
                    payment.setVariableSymbol(node.get("value").asText());
                    continue;
                }

                if (name.equals("KS")) {
                    payment.setConstantSymbol(node.get("value").asText());
                    continue;
                }

                if (name.equals("Zpráva pro příjemce")) {
                    payment.setMessageForRecipient(node.get("value").asText());
                    continue;
                }

                if (name.equals("Komentář")) {
                    payment.setNote(node.get("value").asText());
                }

            }
        }

        if (payment.getVariableSymbol() != null) {
            Optional<User> userOptional = userRepository.findById(Long.parseLong(payment.getVariableSymbol()));
            userOptional.ifPresent(payment::setUser);
        }

        return payment;
    }

}
