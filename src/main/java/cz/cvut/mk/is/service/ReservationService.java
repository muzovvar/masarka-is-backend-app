package cz.cvut.mk.is.service;

import cz.cvut.mk.is.models.Reservation;
import cz.cvut.mk.is.models.ReservationItem;
import cz.cvut.mk.is.repository.ReservationItemRepository;
import cz.cvut.mk.is.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import java.util.List;

@Service
public class ReservationService {

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    ReservationItemRepository reservationItemRepository;

    @Autowired
    TemplateEngine templateEngine;

    @Autowired
    JavaMailSender javaMailSender;

    public List<Reservation> findAllByReservationItemId(long id) {
        ReservationItem reservationItem = this.reservationItemRepository.findById(id).get();
        List<Reservation> reservations = this.reservationRepository.findAllByReservationItem(reservationItem);
        return reservations;
    }

    public Reservation create(Reservation reservation) throws MessagingException {
        reservation = this.reservationRepository.save(reservation);
        this.sendReservationCreationNotificationEmail(reservation);
        return reservation;
    }

    public Reservation update(Reservation oldReservation, Reservation newReservation) {
        oldReservation.setReservationItem(newReservation.getReservationItem());
        oldReservation.setStart(newReservation.getStart());
        oldReservation.setEnd(newReservation.getEnd());
        oldReservation.setUser(newReservation.getUser());
        oldReservation.setNote(newReservation.getNote());
        return this.reservationRepository.save(oldReservation);
    }

    public void delete(Reservation reservation) throws MessagingException {
        this.sendReservationCancellationNotificationEmail(reservation);
        this.reservationRepository.delete(reservation);
    }

    public void sendReservationCreationNotificationEmail(Reservation reservation) throws MessagingException {
        // create thymeleaf context object
        Context context = new Context();
        context.setVariable("reservation", reservation);

        // process email template
        String emailText = templateEngine.process("emails/reservation-creation", context);

        // create new email message object
        javax.mail.internet.MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setSubject("[Masarka Club] New reservation - " +
                reservation.getReservationItem().getName() + " [" +
                reservation.getStart() + " - " + reservation.getEnd() + "]"
        );
        helper.setText(emailText, true);
        helper.setTo(reservation.getUser().getEmail());
        helper.setReplyTo(reservation.getUser().getEmail());

        if (reservation.getReservationItem().getEmail() != null) {
            helper.setBcc(reservation.getReservationItem().getEmail());
        }

        // send email message
        javaMailSender.send(mimeMessage);
    }

    public void sendReservationCancellationNotificationEmail(Reservation reservation) throws MessagingException {
        // create thymeleaf context object
        Context context = new Context();
        context.setVariable("reservation", reservation);

        // process email template
        String emailText = templateEngine.process("emails/reservation-cancellation", context);

        // create new email message object
        javax.mail.internet.MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setSubject("[Masarka Club] Cancelled reservation - " +
                reservation.getReservationItem().getName() + " [" +
                reservation.getStart() + " - " + reservation.getEnd() + "]"
        );
        helper.setText(emailText, true);
        helper.setTo(reservation.getUser().getEmail());
        helper.setReplyTo(reservation.getUser().getEmail());

        if (reservation.getReservationItem().getEmail() != null) {
            helper.setBcc(reservation.getReservationItem().getEmail());
        }

        // send email message
        javaMailSender.send(mimeMessage);
    }

}
