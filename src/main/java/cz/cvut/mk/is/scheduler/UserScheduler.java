package cz.cvut.mk.is.scheduler;

import cz.cvut.mk.is.models.Device;
import cz.cvut.mk.is.models.Payment;
import cz.cvut.mk.is.models.User;
import cz.cvut.mk.is.repository.DeviceRepository;
import cz.cvut.mk.is.repository.PaymentRepository;
import cz.cvut.mk.is.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserScheduler {

    @Autowired
    UserService userService;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    DeviceRepository deviceRepository;


    public LocalDate getPaymentValidityStart() {

        int year = LocalDate.now().getYear();
        int month = LocalDate.now().getMonthValue();

        if (month < 3) {
            return LocalDate.of(year-1, 7, 1);
        }

        if (month < 7) {
            return LocalDate.of(year, 1, 1);
        }

        return LocalDate.of(year, 7, 1);
    }

    public boolean getUserRequiredState(User user) {

        // return "true" if user was registered no more than 14 days ago
        if (Math.abs(ChronoUnit.DAYS.between(user.getRegistrationDatetime(), LocalDateTime.now())) < 14) {
            return true;
        }

        // find all user payments
        List<Payment> payments = this.paymentRepository.findAllByUser(user);

        // filter only valid payments
        payments = payments.stream().filter(
                item -> item.getDate().isAfter(getPaymentValidityStart())
        ).collect(Collectors.toList());

        // get sum of all valid payments
        double sum = payments.stream().mapToDouble(Payment::getAmount).sum();

        // return "true" if total sum is greater than 800 CZK
        if (sum >= 800) {
            return true;
        }

        // return "true" if user has at leaste
        if (user.getRoles().size() > 1) {
            return true;
        }

        // otherwise return "false"
        return false;
    }

    public void updateUserDevicesActiveState(User user, boolean requiredState) {
        List<Device> devices = this.deviceRepository.findAllByUser(user);
        for (Device device : devices) {
            device.setActive(requiredState);
        }
        this.deviceRepository.saveAll(devices);
    }


    @Scheduled(cron = "0 5 * * * ?")
    public void updateUsersActiveState() {

        List<User> users = this.userService.findAll();

        for (User user : users) {

            boolean requiredState = getUserRequiredState(user);

            if (user.isActive() != requiredState) {
                System.out.println(user);
                user.setActive(requiredState);
                this.updateUserDevicesActiveState(user, requiredState);
                this.userService.update(user);
            }
        }
    }
}
