package cz.cvut.mk.is.scheduler;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.cvut.mk.is.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class PaymentScheduler {

    @Autowired
    PaymentService paymentService;

    @Scheduled(cron = "0 0 * * * ?")
    public void fetchPaymentsFromBankAPI() {
        try {
            paymentService.fetchPaymentsFromBankAPI();
            System.out.println("INFO Payments successfully fetched");
        } catch (JsonProcessingException e) {
            System.out.println("ERROR Payments fetching failed");
        }
    }
}
