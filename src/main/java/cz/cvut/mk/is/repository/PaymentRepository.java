package cz.cvut.mk.is.repository;

import cz.cvut.mk.is.models.Payment;
import cz.cvut.mk.is.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    List<Payment> findAllByUser(User user);
    Boolean existsByTransactionId(Long transactionId);
}