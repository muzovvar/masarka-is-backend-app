package cz.cvut.mk.is.repository;

import cz.cvut.mk.is.models.Reservation;
import cz.cvut.mk.is.models.ReservationItem;
import cz.cvut.mk.is.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    List<Reservation> findAllByUser(User user);
    List<Reservation> findAllByReservationItem(ReservationItem reservationItem);

}

