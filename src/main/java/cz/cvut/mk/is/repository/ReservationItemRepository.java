package cz.cvut.mk.is.repository;

import cz.cvut.mk.is.models.ReservationItem;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationItemRepository extends JpaRepository<ReservationItem, Long> {
    Optional<ReservationItem> findReservationItemByName(@NotBlank String reservationItemName);
    List<ReservationItem> findAllByActiveTrue(Sort name);
}