package cz.cvut.mk.is.repository;

import cz.cvut.mk.is.models.Device;
import cz.cvut.mk.is.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {
    List<Device> findAllByUser(User user);
}
