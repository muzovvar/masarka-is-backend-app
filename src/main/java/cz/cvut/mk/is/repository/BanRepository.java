package cz.cvut.mk.is.repository;

import cz.cvut.mk.is.models.Ban;
import cz.cvut.mk.is.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BanRepository extends JpaRepository<Ban, Long> {
    List<Ban> findAllByUser(User user);
}