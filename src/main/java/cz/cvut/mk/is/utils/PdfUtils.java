package cz.cvut.mk.is.utils;

import cz.cvut.mk.is.models.User;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class PdfUtils {

    public static ByteArrayOutputStream generateRegistrationFormPdfFile(User user) throws IOException {

        // Load PDF template
        File file = new File("REG_masarka-11a.pdf");
        PDDocument document = PDDocument.load(file);

        // Retrieve the first page of the document
        PDPage page = document.getPage(0);
        PDPageContentStream contentStream = new PDPageContentStream(document, page,
                PDPageContentStream.AppendMode.APPEND,true,true);

        // Load font file
        PDType0Font font = PDType0Font.load(document, new File("ccll.ttf"));

        // Write user text
        writeText(contentStream, user.getName(), 300, 270, font,12, Color.BLUE);
        writeText(contentStream, user.getSurname(), 300, 250, font,12, Color.BLUE);
        writeText(contentStream, user.getEmail(), 300, 230, font,12, Color.BLUE);
        writeText(contentStream, user.getDateOfBirth().toString(), 300, 210, font,12, Color.BLUE);
        writeText(contentStream, user.getPlaceOfBirth(), 300, 190, font,12, Color.BLUE);
        writeText(contentStream, user.getPermanentAddress(), 300, 170, font,9, Color.BLUE);
        writeText(contentStream, user.getRegistrationDatetime().toString(), 300, 150, font,12, Color.BLUE);
        writeText(contentStream, user.getId().toString(), 300, 130, font,12, Color.BLUE);

        // Closing the content stream
        contentStream.close();

        // Save PDF to output stream
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        document.save(outputStream);

        // Closing the document
        document.close();

        return outputStream;

    }

    public static void writeText(PDPageContentStream contentStream, String text, int tx, int ty,
                                 PDType0Font font, int fontSize, Color color) throws IOException {

        // Begin the Content stream
        contentStream.beginText();
        contentStream.setFont(font, fontSize);
        contentStream.setNonStrokingColor(color);
        contentStream.newLineAtOffset(tx, ty);
        contentStream.showText(text);
        contentStream.endText();
    }

}
