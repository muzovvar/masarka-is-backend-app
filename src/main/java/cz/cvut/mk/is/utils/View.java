package cz.cvut.mk.is.utils;

public class View {
    public static class Public {}
    public static class User extends Public {}
    public static class Admin extends User {}
}
