//package cz.cvut.mk.is.config;
//
//import cz.cvut.mk.is.models.User;
//import cz.cvut.mk.is.repository.UserRepository;
//import cz.cvut.mk.is.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.data.domain.Sort;
//import org.springframework.stereotype.Service;
//
//import javax.mail.MessagingException;
//import java.util.List;
//
//@Service
//public class DBInit implements CommandLineRunner {
//
//
//    @Autowired
//    UserRepository userRepository;
//
//    @Autowired
//    UserService userService;
//
//
//
//    @Override
//    public void run(String... args) throws MessagingException {
//
//        List<User> users = this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
//
//
//        for (User user: users) {
//            if (user.isActive() && user.getId() > 622) {
//                this.userService.sendRegistrationConfirmationEmail(user);
//                System.out.println(user.getId());
//                System.out.println(user.getUsername());
//                System.out.println("-----------------");
//            }
//        }
//
//    }
//}
//
