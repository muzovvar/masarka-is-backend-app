package cz.cvut.mk.is.exceptions;

public class MoreThanThreeDevicesException extends Exception {
    public MoreThanThreeDevicesException() {
        super("Max three devices per user allowed");
    }
}
