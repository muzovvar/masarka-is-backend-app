package cz.cvut.mk.is.exceptions;

public class CreateNewDeviceException extends Exception {
    public CreateNewDeviceException(String message) {
        super(message);
    }
}
