package cz.cvut.mk.is.exceptions;

public class ReservationItemNotFoundException extends Exception {

    public ReservationItemNotFoundException(String reservationItemName) {
        super("Reservation item with name '" + reservationItemName + "' not found.");
    }
}
