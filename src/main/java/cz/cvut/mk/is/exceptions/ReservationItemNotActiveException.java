package cz.cvut.mk.is.exceptions;

public class ReservationItemNotActiveException extends Exception {
    public ReservationItemNotActiveException() {
        super("Reservation item not active");
    }
}