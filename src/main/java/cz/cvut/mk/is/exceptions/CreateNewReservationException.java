package cz.cvut.mk.is.exceptions;

public class CreateNewReservationException extends Exception {
    public CreateNewReservationException(String message) {
        super(message);
    }
}
