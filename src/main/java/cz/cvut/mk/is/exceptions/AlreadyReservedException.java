package cz.cvut.mk.is.exceptions;

public class AlreadyReservedException extends Exception {
    public AlreadyReservedException() {
        super("Item is already reserved for the given time range");
    }
}
