package cz.cvut.mk.is;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableCaching
@EnableScheduling
public class MasarkaISApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasarkaISApplication.class, args);
	}

}
