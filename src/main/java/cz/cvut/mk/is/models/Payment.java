package cz.cvut.mk.is.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "payments")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long transactionId;

    @Column(name = "date")
    private LocalDate date;

    private double amount;

    @Size(max = 50)
    private String accountNumber;

    @Size(max = 50)
    private String bankCode;

    private String variableSymbol;

    private String constantSymbol;

    @Size(max = 250)
    private String messageForRecipient;

    @Size(max = 250)
    private String note;

    @JsonIgnoreProperties({"active", "username", "password", "email", "dateOfBirth",
            "placeOfBirth", "permanentAddress", "roomNumber", "registrationDatetime", "roles" })
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

}
