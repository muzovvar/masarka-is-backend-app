package cz.cvut.mk.is.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bans")
public class Ban {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ban_start", nullable = false)
    private LocalDateTime start;

    @Column(name = "ban_end", nullable = false)
    private LocalDateTime end;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    @Size(max = 250)
    private String note;

}
