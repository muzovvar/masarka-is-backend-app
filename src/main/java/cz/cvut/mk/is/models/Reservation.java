package cz.cvut.mk.is.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "reservations")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "reservation_start", nullable = false)
    private LocalDateTime start;

    @Column(name = "reservation_end", nullable = false)
    private LocalDateTime end;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name="reservation_item_id")
    private ReservationItem reservationItem;

    @Size(max = 250)
    private String note;

}
