package cz.cvut.mk.is.models;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table( name = "reservation_items",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "name")
        })
public class ReservationItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean active;

    @NotBlank
    @Size(max = 50)
    private String name;

    @Size(max = 10)
    private String roomNumber;

    @Email
    private String email;

    @Size(max = 4096)
    @Column(name = "description_text")
    private String descriptionText;

    @Column(name = "max_concurrency")
    private Integer maxConcurrency;

    @Column(name = "max_duration")
    private Integer maxDuration;

    @Max(value = 23)
    @Min(value = 0)
    @Column(name = "availability_start")
    private Integer availabilityStart;

    @Max(value = 23)
    @Min(value = 0)
    @Column(name = "availability_end")
    private Integer availabilityEnd;

}
