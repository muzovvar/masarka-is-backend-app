package cz.cvut.mk.is.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@Entity
@Table( name = "devices",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "mac_address")
        })
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean active;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "mac_address")
    @NotBlank
    @Size(min = 12, max = 12)
    private String macAddress;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

}