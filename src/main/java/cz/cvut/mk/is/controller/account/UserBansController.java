package cz.cvut.mk.is.controller.account;

import cz.cvut.mk.is.models.Ban;
import cz.cvut.mk.is.models.User;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.BanRepository;
import cz.cvut.mk.is.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/account")
public class UserBansController {

    UserRepository userRepository;
    BanRepository banRepository;

    public UserBansController(UserRepository userRepository, BanRepository banRepository) {
        this.userRepository = userRepository;
        this.banRepository = banRepository;
    }

    @GetMapping("/bans")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getAllUserBans(@AuthenticationPrincipal UserDetails userDetails) {
        try {
            // get authenticated user
//            User user = this.userRepository.findByUsername(userDetails.getUsername()).orElse(null);
            User user = this.userRepository.findByEmail(userDetails.getUsername()).orElse(null);

            // find all user bans and filter actives only
            List<Ban> bans = this.banRepository.findAllByUser(user).stream().filter(ban ->
                    ban.getStart().isBefore(LocalDateTime.now()) && ban.getEnd().isAfter(LocalDateTime.now())
            ).collect(Collectors.toList());

            if (bans.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(bans, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
