package cz.cvut.mk.is.controller.account;

import cz.cvut.mk.is.models.Device;
import cz.cvut.mk.is.models.User;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.DeviceRepository;
import cz.cvut.mk.is.repository.UserRepository;
import cz.cvut.mk.is.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/account")
public class UserDevicesController {

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccountService accountService;

    @GetMapping("/devices")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getAllUserDevices(@AuthenticationPrincipal UserDetails userDetails) {
        try {
            // get authenticated user
//            User user = this.userRepository.findByUsername(userDetails.getUsername()).orElse(null);
            User user = this.userRepository.findByEmail(userDetails.getUsername()).orElse(null);

            List<Device> devices = this.deviceRepository.findAllByUser(user);

            if (devices.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(devices, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/devices")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> createUserDevice(@AuthenticationPrincipal UserDetails userDetails,
                                              @RequestBody Device device) {
        try {
            // get authenticated user
//            User user = this.userRepository.findByUsername(userDetails.getUsername()).orElse(null);
            User user = this.userRepository.findByEmail(userDetails.getUsername()).orElse(null);

            return new ResponseEntity<>(this.accountService.createUserDevice(user, device), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/devices/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> updateUserDevice(@AuthenticationPrincipal UserDetails userDetails,
                                              @PathVariable("id") long id,
                                              @RequestBody Device newDevice) {
        try {
            // get authenticated user
//            User user = this.userRepository.findByUsername(userDetails.getUsername()).orElse(null);
            User user = this.userRepository.findByEmail(userDetails.getUsername()).orElse(null);

            Optional<Device> deviceOptional = this.deviceRepository.findById(id);

            if (deviceOptional.isPresent()) {
                Device device = deviceOptional.get();
                if (device.getUser().equals(user)) {
                    device.setName(newDevice.getName());
                    device.setType(newDevice.getType());
                    device.setMacAddress(newDevice.getMacAddress());
                    device.setActive(user.isActive());
                    return new ResponseEntity<>(this.deviceRepository.save(device), HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/devices/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> deleteUserDevice(@AuthenticationPrincipal UserDetails userDetails,
                                              @PathVariable("id") long id) {
        try {
            // get authenticated user
//            User user = this.userRepository.findByUsername(userDetails.getUsername()).orElse(null);
            User user = this.userRepository.findByEmail(userDetails.getUsername()).orElse(null);

            Optional<Device> deviceOptional = this.deviceRepository.findById(id);

            if (deviceOptional.isPresent()) {
                Device device = deviceOptional.get();
                if (device.getUser().equals(user)) {
                    this.deviceRepository.delete(device);
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                } else {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
