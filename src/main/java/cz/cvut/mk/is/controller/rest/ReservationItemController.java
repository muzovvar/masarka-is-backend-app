package cz.cvut.mk.is.controller.rest;

import cz.cvut.mk.is.models.ReservationItem;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.ReservationItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ReservationItemController {

    ReservationItemRepository reservationItemRepository;

    @Autowired
    public ReservationItemController(ReservationItemRepository reservationItemRepository) {
        this.reservationItemRepository = reservationItemRepository;
    }

    @GetMapping("/reservation_items")
    public ResponseEntity<?> getAllReservationItems() {
        try {
            List<ReservationItem> reservationItems = this.reservationItemRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));

            if (reservationItems.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(reservationItems, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/reservation_items/active")
    public ResponseEntity<?> getActiveReservationItems() {
        try {
            List<ReservationItem> reservationItems = this.reservationItemRepository.findAllByActiveTrue(Sort.by(Sort.Direction.ASC, "name"));

            if (reservationItems.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(reservationItems, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/reservation_items/{id}")
    public ResponseEntity<?> getReservationItemById(@PathVariable("id") long id) {

        try {
            Optional<ReservationItem> reservationItemData = this.reservationItemRepository.findById(id);

            if (reservationItemData.isPresent()) {
                ReservationItem reservationItem = reservationItemData.get();
                return new ResponseEntity<>(reservationItem, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/reservation_items")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'CHAIRMAN')")
    public ResponseEntity<?> createReservationItem(@RequestBody ReservationItem reservationItem) {
        try {
            return new ResponseEntity<>(this.reservationItemRepository.save(reservationItem), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/reservation_items/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'CHAIRMAN')")
    public ResponseEntity<?> updateReservationItem(@PathVariable("id") long id, @RequestBody ReservationItem newReservationItem) {
        try {
            Optional<ReservationItem> reservationItemData = this.reservationItemRepository.findById(id);

            if (reservationItemData.isPresent()) {
                ReservationItem oldReservationItem = reservationItemData.get();
                oldReservationItem.setName(newReservationItem.getName());
                oldReservationItem.setActive(newReservationItem.isActive());
                oldReservationItem.setRoomNumber(newReservationItem.getRoomNumber());
                oldReservationItem.setDescriptionText(newReservationItem.getDescriptionText());
                oldReservationItem.setMaxConcurrency(newReservationItem.getMaxConcurrency());
                oldReservationItem.setMaxDuration(newReservationItem.getMaxDuration());
                oldReservationItem.setAvailabilityStart(newReservationItem.getAvailabilityStart());
                oldReservationItem.setAvailabilityEnd(newReservationItem.getAvailabilityEnd());
                oldReservationItem.setEmail(newReservationItem.getEmail());
                return new ResponseEntity<>(this.reservationItemRepository.save(oldReservationItem), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/reservation_items/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'CHAIRMAN')")
    public ResponseEntity<?> deleteReservationItem(@PathVariable("id") long id) {
        try {
            Optional<ReservationItem> reservationItemData = this.reservationItemRepository.findById(id);

            if (reservationItemData.isPresent()) {
                ReservationItem reservationItem = reservationItemData.get();
                this.reservationItemRepository.delete(reservationItem);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
