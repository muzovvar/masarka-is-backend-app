package cz.cvut.mk.is.controller.rest;


import cz.cvut.mk.is.models.User;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.UserRepository;
import cz.cvut.mk.is.service.UserService;
import cz.cvut.mk.is.utils.PdfUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @GetMapping("/users")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'BOARD_MEMBER', 'MODERATOR', 'CHAIRMAN')")
    public ResponseEntity<?> getAllUsers() {
        try {
            List<User> users = this.userService.findAll();

            if (users.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(users, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getUserById(@PathVariable("id") long id) {

        try {
            Optional<User> userData = this.userRepository.findById(id);

            if (userData.isPresent()) {
                User user = userData.get();
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{id}/pdf")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getUserPdfById(@PathVariable("id") long id) {
        try {

            Optional<User> userData = this.userRepository.findById(id);

            if (userData.isPresent()) {
                User user = userData.get();

                ByteArrayOutputStream outputStream = PdfUtils.generateRegistrationFormPdfFile(user);

                String filename = user.getId().toString() + ".pdf";

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_PDF);
                headers.setContentDispositionFormData(filename, filename);
                headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

                return new ResponseEntity<>(outputStream.toByteArray(), headers, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/users")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'CHAIRMAN')")
    public ResponseEntity<?> createUser(@RequestBody User user) {
        try {
            user = this.userService.create(user);
            return new ResponseEntity<>(user, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/users/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'CHAIRMAN')")
    public ResponseEntity<?> updateUser(@PathVariable("id") long id, @RequestBody User newUser) {
        try {
            Optional<User> userData = this.userRepository.findById(id);

            if (userData.isPresent()) {
                User oldUser = userData.get();
                newUser = this.userService.update(oldUser, newUser);
                return new ResponseEntity<>(newUser, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{id}/reset_password")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'CHAIRMAN')")
    public ResponseEntity<?> resetUserPassword(@PathVariable("id") long id) {
        try {
            Optional<User> userData = this.userRepository.findById(id);

            if (userData.isPresent()) {
                User user = userData.get();
                return new ResponseEntity<>(this.userService.resetPassword(user), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/users/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'CHAIRMAN')")
    public ResponseEntity<?> deleteUser(@PathVariable("id") long id) {
        try {
            Optional<User> userData = this.userRepository.findById(id);

            if (userData.isPresent()) {
                User user = userData.get();
//                this.userRepository.delete(user);
                this.userService.delete(user);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
