package cz.cvut.mk.is.controller.account;

import cz.cvut.mk.is.models.User;
import cz.cvut.mk.is.payload.request.account.UpdateProfileRequest;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.*;
import cz.cvut.mk.is.security.services.UserDetailsImpl;
import cz.cvut.mk.is.service.AccountService;
import cz.cvut.mk.is.service.DeviceService;
import cz.cvut.mk.is.service.ReservationService;
import cz.cvut.mk.is.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/account")
public class UserProfileController {

    AccountService accountService;
    UserRepository userRepository;
    UserService userService;
    ReservationRepository reservationRepository;
    ReservationService reservationService;
    ReservationItemRepository reservationItemRepository;
    DeviceRepository deviceRepository;
    DeviceService deviceService;
    PaymentRepository paymentRepository;
    BanRepository banRepository;

    public UserProfileController(AccountService accountService, UserRepository userRepository, UserService userService, ReservationRepository reservationRepository, ReservationService reservationService, ReservationItemRepository reservationItemRepository, DeviceRepository deviceRepository, DeviceService deviceService, PaymentRepository paymentRepository, BanRepository banRepository) {
        this.accountService = accountService;
        this.userRepository = userRepository;
        this.userService = userService;
        this.reservationRepository = reservationRepository;
        this.reservationService = reservationService;
        this.reservationItemRepository = reservationItemRepository;
        this.deviceRepository = deviceRepository;
        this.deviceService = deviceService;
        this.paymentRepository = paymentRepository;
        this.banRepository = banRepository;
    }

    @GetMapping("/profile")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getUserProfile() {
        try {

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();

//            Optional<User> userData = this.userRepository.findByUsername(userDetails.getUsername());
            Optional<User> userData = this.userRepository.findByEmail(userDetails.getUsername());

            if (userData.isPresent()) {
                User user = userData.get();
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/profile")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> updateUserProfile(@Valid @RequestBody UpdateProfileRequest updateProfileRequest) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();

//            Optional<User> userData = this.userRepository.findByUsername(userDetails.getUsername());
            Optional<User> userData = this.userRepository.findByEmail(userDetails.getUsername());

            if (userData.isPresent()) {
                User user = userData.get();
                accountService.updateUser(user, updateProfileRequest);
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
