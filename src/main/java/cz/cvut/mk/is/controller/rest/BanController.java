package cz.cvut.mk.is.controller.rest;

import cz.cvut.mk.is.models.Ban;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.BanRepository;
import cz.cvut.mk.is.service.BanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class BanController {

    BanRepository banRepository;
    BanService banService;

    @Autowired
    public BanController(BanRepository banRepository, BanService banService) {
        this.banRepository = banRepository;
        this.banService = banService;
    }

    @GetMapping("/bans")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getAllBans() {
        try {
            List<Ban> bans = this.banRepository.findAll();

            if (bans.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(bans, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/bans/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getBanById(@PathVariable("id") long id) {

        try {
            Optional<Ban> banOptional = this.banRepository.findById(id);

            if (banOptional.isPresent()) {
                Ban ban = banOptional.get();
                return new ResponseEntity<>(ban, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/bans")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'CHAIRMAN')")
    public ResponseEntity<?> createBan(@RequestBody Ban ban) {
        try {
            return new ResponseEntity<>(this.banRepository.save(ban), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/bans/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'CHAIRMAN')")
    public ResponseEntity<?> updateBan(@PathVariable("id") long id, @RequestBody Ban newBan) {
        try {
            Optional<Ban> banOptional = this.banRepository.findById(id);

            if (banOptional.isPresent()) {
                Ban oldBan = banOptional.get();
                return new ResponseEntity<>(this.banService.update(oldBan, newBan), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/bans/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'CHAIRMAN')")
    public ResponseEntity<?> deleteBan(@PathVariable("id") long id) {
        try {
            Optional<Ban> banOptional = this.banRepository.findById(id);

            if (banOptional.isPresent()) {
                Ban ban = banOptional.get();
                this.banRepository.delete(ban);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
