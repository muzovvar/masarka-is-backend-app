package cz.cvut.mk.is.controller.account;

import cz.cvut.mk.is.models.Payment;
import cz.cvut.mk.is.models.User;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.PaymentRepository;
import cz.cvut.mk.is.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/account")
public class UserPaymentsController {

    UserRepository userRepository;
    PaymentRepository paymentRepository;

    public UserPaymentsController(UserRepository userRepository, PaymentRepository paymentRepository) {
        this.userRepository = userRepository;
        this.paymentRepository = paymentRepository;
    }

    @GetMapping("/payments")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getUserPayments(@AuthenticationPrincipal UserDetails userDetails) {
        try {
            // get authenticated user
//            User user = this.userRepository.findByUsername(userDetails.getUsername()).orElse(null);
            User user = this.userRepository.findByEmail(userDetails.getUsername()).orElse(null);

            List<Payment> payments = this.paymentRepository.findAllByUser(user);
            if (payments.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(payments, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
