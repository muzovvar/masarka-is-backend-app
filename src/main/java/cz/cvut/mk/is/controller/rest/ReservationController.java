package cz.cvut.mk.is.controller.rest;

import cz.cvut.mk.is.models.Reservation;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.ReservationRepository;
import cz.cvut.mk.is.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ReservationController {

    ReservationRepository reservationRepository;
    ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationRepository reservationRepository,
                                 ReservationService reservationService) {
        this.reservationRepository = reservationRepository;
        this.reservationService = reservationService;
    }

    @GetMapping("/reservations")
    public ResponseEntity<?> getAllReservations() {
        try {
            List<Reservation> reservations = this.reservationRepository.findAll();

            if (reservations.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(reservations, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/reservations/reservation_item/{id}")
    public ResponseEntity<?> getReservationsByReservationItem(@PathVariable("id") long id) {
        try {
            List<Reservation> reservations = this.reservationService.findAllByReservationItemId(id);

            if (reservations.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(reservations, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/reservations/{id}")
    public ResponseEntity<?> getReservationsById(@PathVariable("id") long id) {

        try {
            Optional<Reservation> reservationOptional = this.reservationRepository.findById(id);

            if (reservationOptional.isPresent()) {
                Reservation reservation = reservationOptional.get();
                return new ResponseEntity<>(reservation, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/reservations")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'CHAIRMAN')")
    public ResponseEntity<?> createReservation(@RequestBody Reservation reservation) {
        try {
            reservation = this.reservationRepository.save(reservation);
            return new ResponseEntity<>(reservation, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/reservations/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'CHAIRMAN')")
    public ResponseEntity<?> updateReservation(@PathVariable("id") long id, @RequestBody Reservation newReservation) {
        try {
            Optional<Reservation> reservationOptional = this.reservationRepository.findById(id);

            if (reservationOptional.isPresent()) {
                Reservation oldReservation = reservationOptional.get();
                newReservation = this.reservationService.update(oldReservation, newReservation);
                return new ResponseEntity<>(newReservation, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/reservations/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'MODERATOR', 'CHAIRMAN')")
    public ResponseEntity<?> deleteReservation(@PathVariable("id") long id) {
        try {
            Optional<Reservation> reservationOptional = this.reservationRepository.findById(id);

            if (reservationOptional.isPresent()) {
                Reservation reservation = reservationOptional.get();
                this.reservationRepository.delete(reservation);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
