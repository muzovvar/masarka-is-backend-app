package cz.cvut.mk.is.controller.rest;

import cz.cvut.mk.is.models.Registration;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.RegistrationRepository;
import cz.cvut.mk.is.service.RegistrationService;
import cz.cvut.mk.is.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class RegistrationController {

    RegistrationRepository registrationRepository;
    RegistrationService registrationService;
    UserService userService;

    @Autowired
    public RegistrationController(RegistrationRepository registrationRepository, RegistrationService registrationService, UserService userService) {
        this.registrationRepository = registrationRepository;
        this.registrationService = registrationService;
        this.userService = userService;
    }

    /**
     * This function returns a list of all registrations.
     *
     * @return list of registrations
     */
    @GetMapping("/registrations")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getAllRegistrations() {

        try {
            List<Registration> registrations = this.registrationService.findAll();

            if (registrations.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(registrations, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * This function finds and returns registration by given id parameter.
     *
     * @param id Registration ID to be searched
     * @return registration
     */
    @GetMapping("/registrations/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getRegistrationById(@PathVariable("id") long id) {
        try {
            Optional<Registration> registrationData = registrationRepository.findById(id);

            if (registrationData.isPresent()) {
                Registration registration = registrationData.get();
                return new ResponseEntity<>(registration, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * This function creates new registration record.
     *
     * @param registration new registration to be created
     * @return newly created registration object
     */
    @PostMapping("/registrations")
    public ResponseEntity<?> createRegistration(@RequestBody Registration registration) {
        try {
            registration = this.registrationService.create(registration);
            return new ResponseEntity<>(registration, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getLocalizedMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/registrations/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'CHAIRMAN')")
    public ResponseEntity<?> updateRegistration(@PathVariable("id") long id, @RequestBody Registration newRegistration) {
        try {
            Optional<Registration> registrationData = registrationRepository.findById(id);

            if (registrationData.isPresent()) {
                Registration oldRegistration = registrationData.get();
                newRegistration = this.registrationService.update(oldRegistration, newRegistration);
                return new ResponseEntity<>(newRegistration, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/registrations/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'CHAIRMAN')")
    public ResponseEntity<?> deleteRegistration(@PathVariable("id") long id) {
        try {
            Optional<Registration> registrationData = registrationRepository.findById(id);

            if (registrationData.isPresent()) {
                Registration registration = registrationData.get();
                this.registrationRepository.delete(registration);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
