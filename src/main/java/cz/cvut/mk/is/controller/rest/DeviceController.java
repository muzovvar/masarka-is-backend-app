package cz.cvut.mk.is.controller.rest;

import cz.cvut.mk.is.models.Device;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.DeviceRepository;
import cz.cvut.mk.is.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class DeviceController {

    DeviceRepository deviceRepository;
    DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceRepository deviceRepository, DeviceService deviceService) {
        this.deviceRepository = deviceRepository;
        this.deviceService = deviceService;
    }

    @GetMapping("/devices")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getAllDevices() {
        try {
            List<Device> devices = this.deviceRepository.findAll();
            if (devices.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(devices, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devices/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getDeviceById(@PathVariable("id") long id) {
        try {
            Optional<Device> deviceOptional = this.deviceRepository.findById(id);
            if (deviceOptional.isPresent()) {
                Device device = deviceOptional.get();
                return new ResponseEntity<>(device, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/devices")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'CHAIRMAN')")
    public ResponseEntity<?> createDevice(@RequestBody Device device) {
        try {
            return new ResponseEntity<>(this.deviceRepository.save(device), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/devices/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'CHAIRMAN')")
    public ResponseEntity<?> updateDevice(@PathVariable("id") long id, @RequestBody Device newDevice) {
        try {
            Optional<Device> deviceOptional = this.deviceRepository.findById(id);
            if (deviceOptional.isPresent()) {
                Device oldDevice = deviceOptional.get();
                return new ResponseEntity<>(this.deviceService.update(oldDevice, newDevice), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/devices/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'REGISTRAR', 'CHAIRMAN')")
    public ResponseEntity<?> deleteDevice(@PathVariable("id") long id) {
        try {
            Optional<Device> deviceOptional = this.deviceRepository.findById(id);
            if (deviceOptional.isPresent()) {
                Device device = deviceOptional.get();
                this.deviceRepository.delete(device);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}