package cz.cvut.mk.is.controller.rest;

import cz.cvut.mk.is.models.Payment;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.PaymentRepository;
import cz.cvut.mk.is.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class PaymentController {

    PaymentRepository paymentRepository;
    PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentRepository paymentRepository, PaymentService paymentService) {
        this.paymentRepository = paymentRepository;
        this.paymentService = paymentService;
    }

    @GetMapping("/payments")
    @PreAuthorize("hasAnyRole('ADMIN', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getAllPayments() {
        try {
            List<Payment> payments = this.paymentRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));

            if (payments.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(payments, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/payments/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getPaymentById(@PathVariable("id") long id) {
        try {
            Optional<Payment> paymentOptional = this.paymentRepository.findById(id);

            if (paymentOptional.isPresent()) {
                Payment payment = paymentOptional.get();
                return new ResponseEntity<>(payment, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/payments")
    @PreAuthorize("hasAnyRole('ADMIN', 'CHAIRMAN')")
    public ResponseEntity<?> createPayment(@RequestBody Payment payment) {
        try {
            return new ResponseEntity<>(this.paymentRepository.save(payment), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/payments/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'CHAIRMAN')")
    public ResponseEntity<?> updatePayment(@PathVariable("id") long id, @RequestBody Payment newPayment) {
        try {
            Optional<Payment> paymentOptional = this.paymentRepository.findById(id);
            if (paymentOptional.isPresent()) {
                Payment oldPayment = paymentOptional.get();
                return new ResponseEntity<>(this.paymentService.update(oldPayment, newPayment), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/payments/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'CHAIRMAN')")
    public ResponseEntity<?> deletePayment(@PathVariable("id") long id) {
        try {
            Optional<Payment> paymentOptional = this.paymentRepository.findById(id);
            if (paymentOptional.isPresent()) {
                Payment payment = paymentOptional.get();
                this.paymentRepository.delete(payment);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}