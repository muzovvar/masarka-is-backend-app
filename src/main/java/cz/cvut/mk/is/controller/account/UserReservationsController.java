package cz.cvut.mk.is.controller.account;

import cz.cvut.mk.is.models.Reservation;
import cz.cvut.mk.is.models.User;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.ReservationRepository;
import cz.cvut.mk.is.repository.UserRepository;
import cz.cvut.mk.is.service.AccountService;
import cz.cvut.mk.is.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/account")
public class UserReservationsController {


    AccountService accountService;
    UserRepository userRepository;
    ReservationRepository reservationRepository;
    ReservationService reservationService;

    @Autowired
    public UserReservationsController(AccountService accountService,
                                      UserRepository userRepository,
                                      ReservationRepository reservationRepository,
                                      ReservationService reservationService) {
        this.accountService = accountService;
        this.userRepository = userRepository;
        this.reservationRepository = reservationRepository;
        this.reservationService = reservationService;
    }

    @GetMapping("/reservations")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getAllUserReservations(@AuthenticationPrincipal UserDetails userDetails) {
        try {
            // get authenticated user
//            User user = this.userRepository.findByUsername(userDetails.getUsername()).orElse(null);
            User user = this.userRepository.findByEmail(userDetails.getUsername()).orElse(null);

            // find all user user reservations
            List<Reservation> reservations = reservationRepository.findAllByUser(user);

            if (reservations.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(reservations, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/reservations")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> createUserReservation(@AuthenticationPrincipal UserDetails userDetails,
                                                   @RequestBody Reservation reservation) {
        try {
            // get authenticated user
//            User user  = this.userRepository.findByUsername(userDetails.getUsername()).orElse(null);
            User user = this.userRepository.findByEmail(userDetails.getUsername()).orElse(null);

            // create new reservation
            return new ResponseEntity<>(this.accountService.createReservation(user, reservation), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/reservations/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> deleteUserReservation(@AuthenticationPrincipal UserDetails userDetails,
                                                   @PathVariable("id") long id) {
        try {
            // get authenticated user
//            User user = this.userRepository.findByUsername(userDetails.getUsername()).orElse(null);
            User user = this.userRepository.findByEmail(userDetails.getUsername()).orElse(null);

            Optional<Reservation> reservationOptional = this.reservationRepository.findById(id);

            if (reservationOptional.isPresent()) {
                Reservation reservation = reservationOptional.get();
                if (reservation.getUser().equals(user)) {
                    this.reservationService.delete(reservation);
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                } else {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
