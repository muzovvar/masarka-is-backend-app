package cz.cvut.mk.is.controller.rest;

import cz.cvut.mk.is.models.Role;
import cz.cvut.mk.is.payload.response.MessageResponse;
import cz.cvut.mk.is.repository.RoleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class RoleController {

    RoleRepository roleRepository;

    public RoleController(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @GetMapping("/roles")
    @PreAuthorize("hasAnyRole('ADMIN', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getAllRoles() {
        try {
            List<Role> roles = this.roleRepository.findAll();

            if (roles.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(roles, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/roles/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'BOARD_MEMBER', 'CHAIRMAN')")
    public ResponseEntity<?> getRoleById(@PathVariable("id") long id) {
        try {
            Optional<Role> roleOptional = this.roleRepository.findById(id);

            if (roleOptional.isPresent()) {
                Role role = roleOptional.get();
                return new ResponseEntity<>(role, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/roles")
    @PreAuthorize("hasAnyRole('ADMIN', 'CHAIRMAN')")
    public ResponseEntity<?> createRole(@RequestBody Role role) {
        try {
            role = this.roleRepository.save(role);
            return new ResponseEntity<>(role, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/roles/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'CHAIRMAN')")
    public ResponseEntity<?> updateRole(@PathVariable("id") long id, @RequestBody Role newRole) {
        try {
            Optional<Role> roleOptional = this.roleRepository.findById(id);

            if (roleOptional.isPresent()) {
                Role role = roleOptional.get();
                role.setName(newRole.getName());
                role = this.roleRepository.save(role);
                return new ResponseEntity<>(role, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/roles/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'CHAIRMAN')")
    public ResponseEntity<?> deleteRole(@PathVariable("id") long id) {
        try {
            Optional<Role> roleOptional = this.roleRepository.findById(id);

            if (roleOptional.isPresent()) {
                Role role = roleOptional.get();
                this.roleRepository.delete(role);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
