package cz.cvut.mk.is.payload.request.account;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserDeviceRequest {

    @NotBlank
    private String name;

    @NotBlank
    private String macAddress;

}
