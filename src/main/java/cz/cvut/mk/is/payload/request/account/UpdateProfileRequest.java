package cz.cvut.mk.is.payload.request.account;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
public class UpdateProfileRequest {
    @Email
    private String email;

    private String password;

    @Size(max = 20)
    private String roomNumber;
}

