package cz.cvut.mk.is.payload.response;

import lombok.Data;


@Data
public class MessageResponse {
	String message;
	String type;

	public MessageResponse(String message, String type) {
		this.message = message;
		this.type = type;
	}

	public MessageResponse(String message) {
		this.message = message;
		this.type = "error";
	}
}
