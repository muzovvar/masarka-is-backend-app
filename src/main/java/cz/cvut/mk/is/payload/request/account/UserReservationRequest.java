package cz.cvut.mk.is.payload.request.account;

import cz.cvut.mk.is.models.ReservationItem;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


@Data
public class UserReservationRequest {

    @NotNull
    private ReservationItem reservationItem;

    @NotBlank
    private LocalDateTime start;

    @NotBlank
    private LocalDateTime end;

    private String note;

}
