package cz.cvut.mk.is.payload.response;

import lombok.Data;

import java.util.List;

@Data
public class JwtResponse {

	private String accessToken;
	private String type = "Bearer";
	private Long id;
	private String name;
	private String surname;
	private String username;
	private String email;
	private List<String> roles;

	public JwtResponse(String accessToken, Long id, String name, String surname, String username,
					   String email, List<String> roles) {
		this.accessToken = accessToken;
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.username = username;
		this.email = email;
		this.roles = roles;
	}

}
